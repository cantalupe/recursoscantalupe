<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
	protected $fillable = [
        'nombre',
    ];

	public function links ()
   	{
    	return $this->hasMany(Link::class);
    }
}
