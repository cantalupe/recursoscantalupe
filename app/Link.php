<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
	protected $fillable = [
        'link',
        'descripcion', 
        'section_id'
    ];

    public function section ()
    {
    	 return $this->belongsTo(Section::class);
    }

}
