<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Section;
use App\Link;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sections = Section::all();
        $links = Link::all();

        return view('home')->with([
            'sections' => $sections,
            'links' => $links
        ]);
    }
}
