@extends ('layout.main')

@section('content')
<main class="main-container" role="main" >
	<img src="{{ asset('img/logo.png') }}" height="54" width="317" class="logo">
	@guest
	@else
	    <div id="">
        	<a class="btn btn btn-outline-dark m-2" style="background-color:transparent;" data-toggle="modal" data-target="#nuevoLink" data-item-id="TEST">Nuevo Link</a>
        	<a class="btn btn btn-outline-dark m-2" style="background-color:transparent;" data-toggle="modal" data-target="#nuevaSeccion" data-item-id="TEST">Nueva Sección</a>
        	<a class="btn btn btn-outline-dark m-2" style="background-color:transparent;" data-toggle="modal" data-target="#removeLink" data-item-id="TEST">Eliminar Link</a>
        	<a class="btn btn btn-outline-dark m-2" style="background-color:transparent;" data-toggle="modal" data-target="#removeSection" data-item-id="TEST">Eliminar Sección</a>
        </div> 
	@endguest
	@if ($message = Session::get('success'))
	<div class="alert alert-success alert-block">
	  <button type="button" class="close" data-dismiss="alert">×</button>
	  <strong>{{ $message }}</strong>
	</div>
	@endif

	@if ($message = Session::get('error'))
	<div class="alert alert-warning alert-block">
	  <button type="button" class="close" data-dismiss="alert">×</button>
	  <strong>{{ $message }}</strong>
	</div>
	@endif

	@if ($errors->any())
	<div class="alert alert-danger">
	  <ul>
	    @foreach ($errors->all() as $error)
	    <li>{{ $error }}</li>
	    @endforeach
	  </ul>
	</div><br />
	@endif

	<section>
		<div class="container">
			<div class="row">
				@foreach ($sections as $section)	
				<div id="accordion">
					<div class="card m-4">
						<div class="card-header" id="heading{{$section->id}}">
							<h5 class="mb-0">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$section->id}}" aria-expanded="true" aria-controls="collapse{{$section->id}}">
									<h2>{{$section->nombre}}</h2>
								</button>
							</h5>
						</div>
						<div id="collapse{{$section->id}}" class="collapse" aria-labelledby="heading{{$section->id}}" data-parent="#accordion">
							<div class="card-body">
								<ul>
									@foreach ($section->links()->get() as $link)
									<li><a href="{{$link->link}}" target="_blank">{{$link->link}} - {{$link->descripcion}}</a></li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</section>
</main>

@endsection
@include('modal')