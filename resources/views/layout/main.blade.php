<!DOCTYPE html>
<html lang="es">
	@include ('layout.head')
	@yield('head')
	<body class="reloco">
		@include('layout.header')
		@yield ('content')
	</body>
</html>