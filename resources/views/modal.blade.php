{{--MODAL NUEVO LINK--}}
<div class="modal" tabindex="-1" role="dialog" id="nuevoLink">
    <form action="{{ route('link.store') }}" method="POST" enctype="multipart/form-data">   
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="alert alert-danger" style="display:none"></div>
                <div class="modal-header">
                    <h5 class="modal-title">Nuevo Link</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="link">Link:</label>
                            <input type="text" class="form-control" name="link" id="link">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="descripcion">Descripción:</label>
                            <input type="text" class="form-control" name="descripcion" id="descripcion">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="seccion">Sección:</label>
                            <select class="form-control m-bot15" name="section_id">
                              @foreach ($sections as $section)
                                  <option value="{{$section->id}}">{{$section->nombre}}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button  class="btn btn-success" id="editSubmit">Guardar</button>
                </div>
            </div>
        </div>
    </form>
</div>

{{--MODAL ELIMINAR LINK--}}
<div class="modal" tabindex="-1" role="dialog" id="removeLink">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="alert alert-danger" style="display:none"></div>
            <div class="modal-header">
                <h5 class="modal-title">Eliminar Link</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <ul class="list-group">
                        @foreach ($links as $link)
                        <form action="{{ route('link.destroy',$link->id) }}" method="POST" enctype="multipart/form-data">   
                            @csrf
                            {{ method_field('DELETE') }}
                            <li class="list-group-item">
                                {{$link->link}} - {{$link->descripcion}}
                                 <button  class="btn btn-success">Eliminar</button>
                            </li>
                        </form>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>


{{--MODAL NUEVA SECCION--}}
<div class="modal" tabindex="-1" role="dialog" id="nuevaSeccion">
    <form action="{{ route('section.store') }}" method="POST" enctype="multipart/form-data">   
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="alert alert-danger" style="display:none"></div>
                <div class="modal-header">
                    <h5 class="modal-title">Nueva Sección</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="Name">Nombre:</label>
                            <input type="text" class="form-control" name="nombre" id="nombre">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button  class="btn btn-success" id="editSubmit">Guardar</button>
                </div>
            </div>
        </div>
    </form>
</div>

{{--MODAL ELIMINAR SECCION--}}
<div class="modal" tabindex="-1" role="dialog" id="removeSection">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="alert alert-danger" style="display:none"></div>
            <div class="modal-header">
                <h5 class="modal-title">Eliminar Seccion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <ul class="list-group">
                        @foreach ($sections as $section)
                        <form action="{{ route('section.destroy',$section->id) }}" method="POST" enctype="multipart/form-data">   
                            @csrf
                            {{ method_field('DELETE') }}
                            <li class="list-group-item">
                                {{$section->nombre}}
                                 <button  class="btn btn-success">Eliminar</button>
                            </li>
                        </form>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>
